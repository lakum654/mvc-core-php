<?php 

trait View {

    public function view($view,array $data = []){
        if(file_exists('View/'.$view.'.php')) {
            $data;
            include 'View/'.$view.'.php';
        }
    }

}